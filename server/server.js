require( './config/config' )
const express = require( 'express' );
const app = express();
const mongoose = require( 'mongoose' );
const bodyParser = require( 'body-parser' );
const path = require( 'path' );

app.use( bodyParser.urlencoded( { extended: false } ) );
app.use( bodyParser.json() );

app.use( express.static( path.resolve( __dirname, '../public' ) ) );

// configuracion global de rutas
app.use( require( './rutas/index.js' ) );

mongoose.connect( process.env.URL_DB, (err, res) => {
    if( err ) throw err;
    console.log( 'Connect to MongoDB ' + process.env.URL_DB );
} );

app.listen( process.env.PORT, () => {
    console.log( 'Servidor corriendo en el puerto: ' + process.env.PORT );
} );
