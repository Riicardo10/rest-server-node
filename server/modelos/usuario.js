const mongoose = require( 'mongoose' );
const validator = require( 'mongoose-unique-validator' );

let rolesValidos = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} no es un rol válido.'
};

let Schema = mongoose.Schema;

let usuarioSchema = new Schema( {
    nombre: {
        type: String,
        required: [true, 'El nombre del usuario es requerido.']
    },
    email: {
        type: String, 
        required: [true, 'El email del usuario es requerido.'],
        unique: true,
        index: true
    },
    password: {
        type: String,
        required: [true, 'La contrasenia es requerida.']
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: rolesValidos
    },
    estado: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false
    }
} );

usuarioSchema.methods.toJSON = function(){
    let user = this
    let userObject = user.toObject();
    delete userObject.password;
    return userObject;
};

usuarioSchema.plugin( validator, {
    message: '{PATH} debe de ser único.'
} );

module.exports = mongoose.model( 'Usuario', usuarioSchema );