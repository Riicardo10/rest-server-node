const jwt = require( 'jsonwebtoken' );

// ====================================================
// Verificar TOKEN
// ====================================================
let verificaToken = ( req, res, next ) => {
    let token = req.get( 'token' );
    jwt.verify( token, process.env.SEED_TOKEN, (err, decoded) => {
        if( err ) {
            return res.status( 401 ).json( {
                ok: false,
                err: {
                    message: 'Token no válido'
                }
            } );
        }
        req.usuario = decoded.usuario;
        next();
    } );
}

// ====================================================
// Verificar ADMIN_ROL
// ====================================================
let verificaAdminRol = ( req, res, next ) => {
    let rol = req.usuario.role;
    if( rol === 'ADMIN_ROLE' ) {
        next();
    }
    else{
        return res.json( {
            ok: false,
            err: {
                message: 'El usuario no es ADMIN.'
            }
        } );
    }
}

// ====================================================
// Verificar TOKEN_IMG
// ====================================================
let verificaTokenImg = ( req, res, next ) => {
    let token = req.query.token;
    jwt.verify( token, process.env.SEED_TOKEN, (err, decoded) => {
        if( err ) {
            return res.status( 401 ).json( {
                ok: false,
                err: {
                    message: 'Token no válido'
                }
            } );
        }
        req.usuario = decoded.usuario;
        next();
    } );
}

// ====================================================
// EXPORTACION DE LOS MIDDLEWARES
// ====================================================
module.exports = {
    verificaToken,
    verificaAdminRol,
    verificaTokenImg
};