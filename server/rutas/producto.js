const express = require( 'express' );
const app = express();
const { verificaToken, verificaAdminRol } = require( '../middlewares/autenticacion' );
const Producto = require( '../modelos/producto' );

app
    .get( '/producto', verificaToken, (req, res) => {
        let desde = Number( req.query.desde || 0 );
        let limite = Number( req.query.limite || 5 );
        Producto.find( {disponible: true} )
            .skip( desde )
            .limit( limite )
            .populate( 'usuario', 'nombre email role' )          // object _id
            .populate( 'categoria', 'descripcion' )                           // object _id
            .exec( (err, productos) => {
                if(err){
                    return res.status( 400 ).json( {
                        ok: false,
                        err
                    } );
                }
                res.json( {
                    ok: true,
                    productos
                } );
            } );
    } )
    .get( '/producto/:id', verificaToken, (req, res) => {
        let id = req.params.id;
        Producto.findById( id  ) 
            .populate( 'usuario', 'nombre email role' )
            .populate( 'categoria', 'descripcion' )
            .exec( (err, productoDB) => {
            if(err){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                producto: productoDB
            } );
        } )
    } )
    .get( '/producto/buscar/:termino', verificaToken, (req, res) => {
        let termino = req.params.termino;
        let regex = new RegExp( termino, 'i' );
        Producto.find( {nombre: regex} )
            .populate( 'categoria', 'descripcion' )
            .exec( (err, productos) => {
                if(err){
                    return res.status( 500 ).json( {
                        ok: false,
                        err
                    } );
                }
                res.json( {
                    ok: true,
                    productos
                } );
            } );
    } )
    .post( '/producto', verificaToken, (req, res) => {
        let body = req.body;
        let producto_JSON = {
            nombre: body.nombre,
            precioUnitario: body.precioUnitario,
            descripcion: body.descripcion,
            categoria: req.body.categoria,
            usuario: req.body.usuario
        };
        let producto = new Producto( producto_JSON );
        producto.save( (err, productoDB) => {
            if(err){
                return res.status( 500 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                producto: productoDB
            } );
        } );
    } )
    .put( '/producto/:id', verificaToken, (req, res) => {
        let id = req.params.id;
        let body = req.body;
        Producto.findByIdAndUpdate( id, body, {new: true, runValidators: true}, (err, productoDB) => {
            if(err){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                producto: productoDB
            } );
        } )
     } )
    .delete( '/producto/:id', [verificaToken, verificaAdminRol], (req, res) => {
        let id = req.params.id;
        const cambiarEstado = {
            disponible: false
        }
        Producto.findByIdAndUpdate( id, cambiarEstado, {new: true}, (err, productoAgotado) => {
            if(err){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                producto: productoAgotado,
                mensaje: 'Producto agotado'
            } );
        } )
    } );

    
    











module.exports = app;