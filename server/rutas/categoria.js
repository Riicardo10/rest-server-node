const express = require( 'express' );
const app = express();
const {verificaToken, verificaAdminRol} = require( '../middlewares/autenticacion' );

let Categoria = require( '../modelos/categoria' );

app
    .get( '/categoria', verificaToken, (req, res) => {
        Categoria.find( {} )
            .sort( 'descripcion' )
            .populate( 'usuario', 'nombre email estado role' )          // object _id
            .exec( (err, categorias) => {
                if(err){
                    return res.status( 400 ).json( {
                        ok: false,
                        err
                    } );
                }
                res.json( {
                    ok: true,
                    categorias
                } );
            } );
    } )
    .get( '/categoria/:id', verificaToken, (req, res) => {
        let id = req.params.id;
        Categoria.findById( id, (err, categoriaDB) => {
            if(err){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                categoria: categoriaDB
            } );
        } )
    } )
    .post( '/categoria', verificaToken, (req, res) => {
        let body = req.body;
        let categoria_JSON = {
            descripcion: body.descripcion,
            usuario: req.usuario._id
        };
        let categoria = new Categoria( categoria_JSON );
        categoria.save( (err, categoriaDB) => {
            if(err){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                categoria: categoriaDB
            } );
        } );
    } )
    .put( '/categoria/:id', verificaToken, (req, res) => {
        let id = req.params.id;
        let descripcion = req.body.descripcion;
        Categoria.findByIdAndUpdate( id, {'descripcion': descripcion}, {new: true, runValidators: true}, (err, categoriaDB) => {
            if(err){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                categoria: categoriaDB
            } );
        } )
     } )
    .delete( '/categoria/:id', [verificaToken, verificaAdminRol], (req, res) => {
        let id = req.params.id;
        Categoria.findByIdAndRemove( id, (err, categoriaDB) => {
            if( err ){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            if( !categoriaDB ) {
                return res.status( 400 ).json( {
                    ok: false,
                    err: {
                        message: 'Categoria no encontrado.'
                    }
                } );
            }
            res.json( {
                ok: true,
                message: 'Categoria eliminada',
                categoria: categoriaDB
            } );
        } );
    } );


module.exports = app;