const express = require( 'express' );
const app = express();
const bcrypt = require( 'bcrypt' );
const _ = require( 'underscore' );
const Usuario = require( '../modelos/usuario' );
const {verificaToken, verificaAdminRol} = require( '../middlewares/autenticacion' );

app
    .get( '/', (req, res) => {
        res.json( 'Bienvenido Ricardo Flores: Desarrollo' );
    } )
    .get( '/usuario', verificaToken, (req, res) => {

        /*return res.json( {
            usuario: req.usuario,
            nombre:  req.usuario.nombre,
            email:  req.usuario.email
        } );*/

        let desde = Number( req.query.desde || 0 );
        let limite = Number( req.query.limite || 5 );
        Usuario.find( )
            .skip( desde )
            .limit( limite )
            .exec( (err, usuarios) => {
                if(err){
                    return res.status( 400 ).json( {
                        ok: false,
                        err
                    } );
                }
                Usuario.count({}, (err, contador) => {
                    res.json( {
                        ok: true,
                        usuarios,
                        contador
                    } );
                })
            } );
    } )
    .get( '/usuario/role/:role', (req, res) => {
        let role = (req.params.role).toUpperCase();
        let desde = Number( req.query.desde || 0 );
        let limite = Number( req.query.limite || 5 );
        Usuario.find( {role}, 'nombre email estado role estado' )
            .skip( desde )
            .limit( limite )
            .exec( (err, usuarios) => {
                if(err){
                    return res.status( 400 ).json( {
                        ok: false,
                        err
                    } );
                }
                Usuario.count({role}, (err, contador) => {
                    res.json( {
                        ok: true,
                        usuarios,
                        contador,
                        conteo: usuarios.length
                    } );
                })
            } );
    } )
    .get( '/usuarios', (req, res) => {
        let desde = Number( req.query.desde || 0 );
        let limite = Number( req.query.limite || 5 );
        let estado = {
            estado: req.query.activo || true
        };
        Usuario.find( estado, 'nombre email estado role estado' )
            .skip( desde )
            .limit( limite )
            .exec( (err, usuarios) => {
                if(err){
                    return res.status( 400 ).json( {
                        ok: false,
                        err
                    } );
                }
                Usuario.count(estado, (err, contador) => {
                    res.json( {
                        ok: true,
                        usuarios,
                        contador,
                        conteo: usuarios.length
                    } );
                })
            } );
    } )
    .post( '/usuario', [verificaToken, verificaAdminRol], (req, res) => {
        let body = req.body;
        let usuario_JSON = {
            nombre: body.nombre,
            email: body.email,
            password: bcrypt.hashSync( body.password, 10 ),
            role: body.role
        };
        let usuario = new Usuario( usuario_JSON );
        usuario.save( (err, usuarioDB) => {
            if(err){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                usuario: usuarioDB
            } );
        } );
    } )
    .put( '/usuario/:id', [verificaToken, verificaAdminRol], (req, res) => {
        let id = req.params.id;
        //let body = req.body;
        let body = _.pick( req.body, ['nombre', 'email', 'img', 'role', 'estado'] );

        //delete body.password;
        //delete body.google;

        Usuario.findByIdAndUpdate( id, body, {new: true, runValidators: true}, (err, usuarioDB) => {
            if(err){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                usuario: usuarioDB
            } );
        } )
    } )
    .delete( '/usuario/:id', [verificaToken, verificaAdminRol], (req, res) => {
        // ELIMINACION POR STATUS
        /*let id = req.params.id;
        const cambiarEstado = {
            estado: false
        }
        Usuario.findByIdAndUpdate( id, cambiarEstado, {new: true}, (err, usuarioInactivo) => {
            if(err){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            res.json( {
                ok: true,
                usuario: usuarioInactivo
            } );
        } )*/
        // ELIMINACION FISICA
        let id = req.params.id;
        Usuario.findByIdAndRemove( id, (err, usuarioBorrado) => {
            if( err ){
                return res.status( 400 ).json( {
                    ok: false,
                    err
                } );
            }
            if( !usuarioBorrado ) {
                return res.status( 400 ).json( {
                    ok: false,
                    err: {
                        message: 'Usuario no encontrado.'
                    }
                } );
            }
            res.json( {
                ok: true,
                usuario: usuarioBorrado
            } );
        } );
    } )

module.exports = app;