const express = require( 'express' );
const app = express();
const path = require( 'path' );
const fs = require( 'fs' );
const { verificaTokenImg } = require( '../middlewares/autenticacion' );

app
    .get( '/imagen/:tipo/:img', verificaTokenImg, ( req, res ) => {
        let tipo = req.params.tipo;
        let img = req.params.img;

        let pathImagen = path.resolve( __dirname, '../../uploads/' + tipo + '/' + img );
        if( fs.existsSync(pathImagen) ) {
            res.sendFile( pathImagen );
        }
        else {
            const noImage = path.resolve( __dirname, '../assets/no-image.jpg' );
            res.sendFile( noImage );
        }
    } );

module.exports = app;
