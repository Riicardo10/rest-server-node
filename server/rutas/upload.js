const express = require( 'express' );
const app = express();
const fileUpload = require( 'express-fileupload' );
const Usuario = require( '../modelos/usuario' );
const Producto = require( '../modelos/producto' );
const fs = require( 'fs' );
const path = require( 'path' );

app.use( fileUpload() );

app.put( '/upload/:tipo/:id', (req, res) => {

    let tipo = req.params.tipo;
    let id = req.params.id;

    if( !req.files ) {
        return res.status( 400 ).json( {
            ok: false,
            err: {
                message: 'No existe ningun archivo'
            }
        } );
    }

    let tiposValidos = [ 'productos', 'usuarios'];
    if( tiposValidos.indexOf( tipo ) < 0 ){
        return res.status( 400 ).json( {
            ok: false,
            err: {
                message: 'Error. Los tipos permitidos son: ' + tiposValidos.join( ', ' ),
            }
        } );
    }

    let extensionesValidas = [ 'png', 'jpg', 'gif', 'jpeg' ];
    let archivo = req.files.archivo;
    let splits = archivo.name.split( '.' );
    let extension = splits[ splits.length -1 ];
    if( extensionesValidas.indexOf( extension ) < 0 ){
        return res.status( 400 ).json( {
            ok: false,
            err: {
                message: 'Error. Los archivos permitidos a subir son de extensiones: ' + extensionesValidas.join( ', ' ),
                extension
            }
        } );
    }

    let nombreArchivoGenerado = id + '-' + new Date().getMilliseconds() + '.' + extension;
    
    archivo.mv( 'uploads/' + tipo + '/' + nombreArchivoGenerado, (err) => {
        if( err ) {
            return res.status( 500 ).json( {
                ok: false,
                err
            } );
        }
    
        if( tipo === 'usuarios' ) {
            imagenUsuario( id, res, nombreArchivoGenerado );
        }
        else if( tipo === 'productos' ) {
            imagenProducto( id, res, nombreArchivoGenerado );
        }
    } );
} );

let imagenUsuario = (id, res, nombreArchivoGenerado) => {
    Usuario.findById( id, (err, usuarioDB) => {
        if( err ) {
            borrarArchivo( nombreArchivoGenerado, 'usuarios' );
            return res.status( 500 ).json( {
                ok: false,
                err
            } );
        }
        if( !usuarioDB ) {
            borrarArchivo( nombreArchivoGenerado, 'usuarios' );
            return res.status( 400 ).json( {
                ok: false,
                err: {
                    message: 'El usuario no existe'
                }
            } );
        }

        borrarArchivo( usuarioDB.img, 'usuarios' );
        
        usuarioDB.img = nombreArchivoGenerado;
        usuarioDB.save( (err, usuarioActualizado) => {
            return res.json( {
                ok: true,
                usuarioActualizado,
                img: nombreArchivoGenerado
            } );
        } );
    } );
}

let imagenProducto = (id, res, nombreArchivoGenerado) => {
    Producto.findById( id, (err, productoDB) => {
        if( err ) {
            borrarArchivo( nombreArchivoGenerado, 'productos' );
            return res.status( 500 ).json( {
                ok: false,
                err
            } );
        }
        if( !productoDB ) {
            borrarArchivo( nombreArchivoGenerado, 'productos' );
            return res.status( 400 ).json( {
                ok: false,
                err: {
                    message: 'El producto no existe'
                }
            } );
        }

        borrarArchivo( productoDB.img, 'productos' );
        
        productoDB.img = nombreArchivoGenerado;
        productoDB.save( (err, productoActualizado) => {
            return res.json( {
                ok: true,
                productoActualizado,
                img: nombreArchivoGenerado
            } );
        } );
    } );
}

let borrarArchivo = ( nombreArchivo, tipo ) => {
    let pathImagen = path.resolve( __dirname, '../../uploads/' + tipo + '/' + nombreArchivo );
    if( fs.existsSync( pathImagen ) ) {
        fs.unlinkSync( pathImagen );
    }
}

module.exports = app;